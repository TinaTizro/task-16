
const elBtnWork = document.getElementById('btn-work');
const elBtnGetLoan = document.getElementById('btn-bank-loan');
const elBtnBuy = document.getElementById('btn-buy');
const elBtnBankBalance = document.getElementById('bank-balance');
const elComputer = document.getElementById('computer');

let bankBalance;
let loanAmount;
let givenLoan;
let computerCost

// Variables
const HOURLY_RATE = 30;
const MIN_BALANCE_FOR_LOAN = 200;
const MAX_NUM_OF_LOAN = 1;

//attach a click event to a button
// element.addEventListener("click", myFunction);

elBtnWork.addEventListener('click', doWork);
elBtnGetLoan.addEventListener('click', getLoan);
elBtnBuy.addEventListener('click', buyComputer);
elComputer.addEventListener('change', onComputerChanged);


//he parseFloat() function parses a string and returns a floating point number. parseFloat(string).
//parseInt(string, radix)
function init() {
    console.log('Initializing program...');
    
    givenLoan = 0;
    loadAmount = 0;
    computerCost = parseInt(elBtnBankBalance.value);
    bankBalance = parseFloat(elBtnBankBalance.innerText);

}
function onComputerChanged() {
    computerCost = parseInt(this.value);
    console.log('changed computer' + computerCost);

}

function doWork() {
    bankBalance += HOURLY_RATE;
    elBtnBankBalance.innerText = bankBalance;
}

function getLoan() {
    if (givenLoan >= MAX_NUM_OF_LOAN) {
        alert('You already received your loan😁');
        return;
    }
    if (bankBalance < MIN_BALANCE_FOR_LOAN) {
        alert('Sorry! You need to work more');
        return;
    }
    const acceptLoan = confirm(`You are about to get a loan for ${computerCost} kr. Do you wish to 
    continue?`);
    console.log(acceptLoan);


    if (acceptLoan === true) {
        console.log('You have accepted loan...');
        givenLoan += 1;
        bankBalance += computerCost;
        elBtnBankBalance.innerText = bankBalance;
    }
}

//Question
function buyComputer() {
  if (bankBalance <= computerCost) {
      bankBalance -= computerCost;
      elBtnBankBalance.innerText = bankBalance;
      alert('Congratulats you will get your computer within few days.');
  } else {
      alert('Sorry! You cannot continue shopping in this store.');
      
  }

  
}